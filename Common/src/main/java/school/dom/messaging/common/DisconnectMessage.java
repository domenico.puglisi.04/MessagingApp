package school.dom.messaging.common;

public class DisconnectMessage extends Message {
    public DisconnectMessage(String from) {
        super(from);
    }

    @Override
    public String toString() {
        return "DisconnectMessage{" +
                "usernameFrom='" + usernameFrom + '\'' +
                ", bValid=" + bValid +
                ", dateTime=" + dateTime +
                '}';
    }

    // no body needs to be specified for a disconnect request
}
