package school.dom.messaging.common;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

public class RemoteConnection {
    public RemoteConnection(String hostname, int port) throws IOException {
        LOGGER.finest("Opening connection to " + hostname + ":" + port);
        remote = new Socket(hostname, port);
        setupSerializeStreams();
    }

    public RemoteConnection(Socket remote) throws IOException {
        LOGGER.finest("Opening connection to " + remote);
        this.remote = remote;
        setupSerializeStreams();
    }

    @Override
    public String toString() {
        return remote.toString();
    }

    public boolean isConnected() {
        return remote.isConnected();
    }

    private void setupSerializeStreams() throws IOException {
        serializeOut = new ObjectOutputStream(remote.getOutputStream());
        serializeOut.flush(); // OutStream on both ends MUST be flushed first
        serializeIn  = new ObjectInputStream(remote.getInputStream());
    }

    public void send(Object obj) throws IOException {
        // LOGGER.fine("Sending " + obj + " to " + remote);
        serializeOut.writeObject(obj);
    }

    public Object receive() throws IOException, ClassNotFoundException {
        Object obj = serializeIn.readObject();
        LOGGER.finest("Received " + obj + " from " + remote);
        return obj;
    }

    public void flush() throws IOException {
        serializeOut.flush();
    }

    public void close() throws IOException {
        LOGGER.finest("Closing connection to " + remote);
        serializeIn.close();
        serializeOut.close();
        remote.close();
    }

    private final Socket remote;
    protected ObjectOutput serializeOut;
    protected ObjectInput serializeIn;

    private static final Logger LOGGER = Logger.getLogger(RemoteConnection.class.getName());
}
