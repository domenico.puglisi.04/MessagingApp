package school.dom.messaging.common;

public class ConnectMessage extends Message {
    public ConnectMessage(String username) {
        // we make use of a known value to form a message
        // even without having estabilished a proper UID before
        super(MessagingConstants.UID_GENERIC_CLIENT);
        requestedUsername = username;

        clientVersion = MessagingConstants.CLIENT_VERSION;
    }

    @Override
    public String toString() {
        return "ConnectMessage{" +
                "requestedUsername='" + requestedUsername + '\'' +
                ", clientVersion=" + clientVersion +
                ", usernameFrom='" + usernameFrom + '\'' +
                ", bValid=" + bValid +
                ", dateTime=" + dateTime +
                '}';
    }

    public String getRequestedUsername() {
        return requestedUsername;
    }

    public int getClientVersion() {
        return clientVersion;
    }

    protected String requestedUsername;
    protected int    clientVersion;
}
