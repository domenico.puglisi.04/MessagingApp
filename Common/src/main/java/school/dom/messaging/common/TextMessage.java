package school.dom.messaging.common;

// TODO: Implement custom exception to prevent construction of
//  exceedingly large Message?

public class TextMessage extends Message {
    public TextMessage(String from, String to, String msg) {
        super(from);
        usernameTo = to;
        buffer = msg;
    }

    @Override
    public String toString() {
        return "TextMessage{" +
                "usernameFrom='" + usernameFrom + '\'' +
                ", bValid=" + bValid +
                ", usernameTo='" + usernameTo + '\'' +
                ", buffer='" + buffer + '\'' +
                '}';
    }

    @Override
    public boolean matchesUser(String username) {
        return super.matchesUser(username) || username.equals(usernameTo);
    }

    public String toDisplayString() {
        return toInfoDisplayString() + ": " + getBuffer();
    }

    public String toInfoDisplayString() {
        return String.format(
                "[%s] @%s",
                MessagingConstants.MSG_DATE_FORMATTER.format(dateTime),
                usernameFrom
        );
    }

    public int getUidTo() {
        return usernameTo.hashCode();
    }

    public String getUsernameTo() {
        return usernameTo;
    }

    public String getBuffer() {
        return buffer;
    }

    public void setBuffer(String buffer) {
        this.buffer = buffer;
    }

    protected final String usernameTo;
    protected String buffer;
}
