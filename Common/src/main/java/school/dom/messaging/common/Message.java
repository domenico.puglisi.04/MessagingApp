package school.dom.messaging.common;

import java.io.Serializable;
import java.time.ZonedDateTime;

public abstract class Message implements Serializable {
    public Message(String from) {
        usernameFrom = from;
        bValid = true;
        dateTime = ZonedDateTime.now();
    }

    @Override
    public String toString() {
        return "Message{" +
                "usernameFrom='" + usernameFrom + '\'' +
                ", bValid=" + bValid +
                ", dateTime=" + dateTime +
                '}';
    }

    public boolean matchesUser(String username) {
        return username.equals(usernameFrom);
    }

    public int getUidFrom() {
        return usernameFrom.hashCode();
    }

    public String getUsernameFrom() {
        return usernameFrom;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public boolean isValid() {
        return bValid;
    }

    public void setValid(boolean v) {
        bValid = v;
    }

    protected final String usernameFrom;
    protected boolean bValid;
    protected final ZonedDateTime dateTime;
}
