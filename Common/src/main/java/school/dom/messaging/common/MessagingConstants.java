package school.dom.messaging.common;

import java.time.format.DateTimeFormatter;

public class MessagingConstants {
    public static final String UID_SERVER = "SERVER";
    public static final String UID_GENERIC_CLIENT = "CLIENT";
    public static final String BROADCAST = "BROADCAST";
    public static final int    CLIENT_VERSION = 1;
    public static final int    MAX_TEXT_LENGTH   = 1024 * 4;
    public static final int    SERVER_MAX_CONNECTIONS = 6;
    public static final int    PORT = 7447;

    public static final DateTimeFormatter MSG_DATE_FORMATTER
            = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O");
}
