package school.dom.messaging.common;

public class ResponseMessage extends Message {
    public ResponseMessage(ResponseCode code) {
        super(MessagingConstants.UID_SERVER);
        responseCode = code;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "responseCode=" + responseCode +
                '}';
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }

    protected final ResponseCode responseCode;
}
