package school.dom.messaging.server;

import school.dom.messaging.common.MessagingConstants;
import school.dom.messaging.common.TextMessage;

import java.io.IOException;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

public class WriterServer implements Runnable {
    public WriterServer(ConcurrentMap<String, ConnectionContainer> pool,
                        BlockingQueue<TextMessage> queue) {
        threadPool = pool;
        outgoingMsgQueue = queue;
    }

    @Override
    public void run() {
        while(true) {
            // Process Queue until empty
            try {
                TextMessage msg = outgoingMsgQueue.take();
                if(msg.isValid()) {
                    switch (msg.getUsernameTo()) {
                        case MessagingConstants.BROADCAST -> handleBroadcast(msg);
                        default -> handleOneToOne(msg);
                    }
                }
            } catch (InterruptedException ignored) {
                // pass
            }
        }
    }

    private void handleBroadcast(TextMessage msg) {
        threadPool.values().forEach(cc -> {
            // Filter out sender, then forward message to all
            if(!cc.getUsername().equals(msg.getUsernameFrom())) {
                try {
                    cc.getClient().send(msg);
                    cc.getClient().flush();
                } catch (IOException e) {
                    LOGGER.warning("Exception occurred when forwarding message to @"
                            + cc.getUsername() + ": " + e.getMessage());
                }
            }
        });
    }

    private void handleOneToOne(TextMessage msg) {
        ConnectionContainer cc = threadPool.get(msg.getUsernameTo());
        if(cc != null) {
            try {
                cc.getClient().send(msg);
                cc.getClient().flush();
            } catch (IOException e) {
                LOGGER.warning("Exception occurred when forwarding message to @"
                        + cc.getUsername() + ": " + e.getMessage());
            }
        } else {
            LOGGER.info("Could not send message to non-existent client " + msg.getUsernameTo());
        }
    }

    protected final ConcurrentMap<String, ConnectionContainer> threadPool;
    protected final BlockingQueue<TextMessage> outgoingMsgQueue;

    private static final Logger LOGGER = Logger.getLogger(WriterServer.class.getName());
}
