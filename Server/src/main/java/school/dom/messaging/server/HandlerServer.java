package school.dom.messaging.server;

import school.dom.messaging.common.*;

import java.io.IOException;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import java.net.*;
import java.util.logging.Logger;

public class HandlerServer implements Runnable {
    public HandlerServer(ServerSocket ss) {
        threadPool = new ConcurrentHashMap<>(MessagingConstants.SERVER_MAX_CONNECTIONS);
        outgoingMsg = new LinkedBlockingQueue<>();
        msgWriterThread = new Thread(new WriterServer(threadPool, outgoingMsg));
        serverSocket = ss;
    }

    @Override
    public void run() {
        // Start the thread which handles the forwarding of messages between clients
        msgWriterThread.start();

        // Logic for accepting new connections from clients
        while(true) {
            cleanupThreadPool();
            try {
                RemoteConnection client = new RemoteConnection(serverSocket.accept());
                if(!registerClient(client)) {
                    client.close();
                }
            } catch (ClassNotFoundException | IOException e) {
                LOGGER.warning("Exception occurred while registering client: " + e.getMessage());
            }
        }
    }

    private boolean registerClient(RemoteConnection client)
            throws IOException, ClassNotFoundException {
        ConnectMessage req = (ConnectMessage) client.receive();
        String username = req.getRequestedUsername();

        ResponseMessage response;

        cleanupThreadPool();

        if(isUsernameValid(username) && !isMaxCapacity()) {
            // Allocate new Listener thread for dedicated monitoring of a connection to client
            // Split allocation and insertion for easier reading of the code
            Thread t = new Thread(new ListenerServer(client, username, outgoingMsg));
            threadPool.put(username, new ConnectionContainer(t, client, username));

            // Starts the allocated thread and sends back a positive response to client
            t.start();
            response = new ResponseMessage(ResponseCode.OK);
            LOGGER.info("Connection estabilished with " + client);
        } else {
            response = new ResponseMessage(ResponseCode.DENY);
        }

        client.send(response);

        return response.getResponseCode() == ResponseCode.OK;
    }

    private boolean isUsernameValid(String username) {
        // i really should have used regex for this
        // but i can't be bothered with only three hours from the deadline
        return !threadPool.containsKey(username) && !username.contains(":") && !username.contains(" ")
                && !(username.length() > 12) && !username.isEmpty()
                && !username.equalsIgnoreCase(MessagingConstants.BROADCAST)
                && !username.equalsIgnoreCase(MessagingConstants.UID_SERVER)
                && !username.equalsIgnoreCase(MessagingConstants.UID_GENERIC_CLIENT);
    }

    private boolean isMaxCapacity() {
        return threadPool.size() >= MessagingConstants.SERVER_MAX_CONNECTIONS;
    }

    private void cleanupThreadPool() {
        LOGGER.fine("Cleaning up thread pool");
        threadPool.entrySet().removeIf(
            entry -> {
                boolean retval = entry.getValue().getState() == Thread.State.TERMINATED;
                // Log if necessary
                if(retval)
                    LOGGER.finer("Deleting connection with " + entry.getKey());
                return retval;
        });
    }

    private final ServerSocket serverSocket;
    private final ConcurrentMap<String, ConnectionContainer> threadPool;
    private final BlockingQueue<TextMessage> outgoingMsg;
    private final Thread msgWriterThread;

    private static final Logger LOGGER = Logger.getLogger(HandlerServer.class.getName());
}
