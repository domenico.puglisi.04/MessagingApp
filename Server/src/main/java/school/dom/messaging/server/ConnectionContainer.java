package school.dom.messaging.server;

import school.dom.messaging.common.RemoteConnection;

public class ConnectionContainer {
    public ConnectionContainer(Thread t, RemoteConnection client, String username) {
        this.t = t;
        this.client = client;
        this.username = username;
    }

    public void start() {
        t.start();
    }

    public Thread.State getState() {
        return t.getState();
    }

    public String getUsername() {
        return username;
    }

    public RemoteConnection getClient() {
        return client;
    }

    protected final Thread t;
    protected final RemoteConnection client;
    protected final String username;
}
