package school.dom.messaging.server;

import school.dom.messaging.common.*;

import java.io.IOException;
import java.net.*;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessagingServer {
    public static void main(String[] args) {
        // Logger config
        PARENT_LOGGER.setLevel(Level.ALL);

        FileHandler fileErr;
        try {
            fileErr = new FileHandler("%t/MessagingServer%g.log");
            fileErr.setLevel(Level.ALL);
        } catch (IOException e) {
            System.err.println("COULD NOT CREATE LOG FILE. TERMINATING PROCESS.");
            return;
        }

        PARENT_LOGGER.addHandler(fileErr);

        // Initialize server socket to then pass to the Handler thread
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(MessagingConstants.PORT);
        } catch (IOException e) {
            System.err.println("COULD NOT OPEN PORT " + MessagingConstants.PORT + ". TERMINATING PROCESS.");
            return;
        }

        // Run server request handling logic
        PARENT_LOGGER.info("Starting application server...");
        HandlerServer handlerServer = new HandlerServer(serverSocket);
        handlerServer.run();
        PARENT_LOGGER.info("Closing application server...");

        // Cleanup before closing application
        fileErr.flush();
    }

    private static final Logger PARENT_LOGGER = Logger.getLogger(MessagingServer.class.getPackageName());
}
