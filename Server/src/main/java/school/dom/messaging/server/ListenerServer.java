package school.dom.messaging.server;

import java.io.EOFException;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import school.dom.messaging.common.*;

public class ListenerServer implements Runnable {
    public ListenerServer(RemoteConnection client, String username,
                          BlockingQueue<TextMessage> outputQueue) {
        this.client = client;
        this.username = username;
        outgoingMsgQueue = outputQueue;
    }

    @Override
    public void run() {
        boolean shouldClose = false;
        while (!shouldClose) {
            try {
                Message received = (Message) client.receive();
                LOGGER.info("Received message " + received);

                if (received instanceof TextMessage msg) {
                    if(!msg.getBuffer().isEmpty()) // ignore messages with empty bodies
                        outgoingMsgQueue.add(msg);
                } else if (received instanceof DisconnectMessage) {
                    shouldClose = true;
                    // tag all orphaned messages as NOT to be sent
                    outgoingMsgQueue.stream()
                            .filter(msg -> msg.isValid() && msg.matchesUser(username))
                            .forEach(msg -> msg.setValid(false));
                }
            } catch (EOFException | SocketException e) {
                shouldClose = true;
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.warning("Exception occurred when expecting input; received invalid data: " + e.getMessage());
                shouldClose = true;
            }
        }

        try {
            LOGGER.info("Connection closed with " + client);
            client.close();
        } catch (IOException ignored) {
            // pass
        }
    }

    private final String username;
    private final RemoteConnection client;
    private final BlockingQueue<TextMessage> outgoingMsgQueue;

    private static final Logger LOGGER = Logger.getLogger(ListenerServer.class.getName());
}
