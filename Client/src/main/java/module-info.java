module school.dom.messaging.client {
    requires java.logging;
    requires school.dom.messaging.common;

    exports school.dom.messaging.client;
}
