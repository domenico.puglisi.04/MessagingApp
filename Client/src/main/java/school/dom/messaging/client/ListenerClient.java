package school.dom.messaging.client;

import school.dom.messaging.common.*;

import java.io.EOFException;
import java.io.IOException;

import java.net.SocketException;
import java.util.function.Function;

public class ListenerClient implements Runnable {
    public ListenerClient(ConnectionClient client, Function<TextMessage, Void> msgHandler) {
        this.client = client;
        this.msgHandler = msgHandler;
    }

    @Override
    public void run() {
        boolean shouldClose = false;
        while(!shouldClose) {
            try {
                Message received = client.receive();
                if (received instanceof TextMessage msg)
                    msgHandler.apply(msg);
                else
                    System.err.println("Received a message that could not be handled; " + received);
            } catch(EOFException | SocketException e) {
                shouldClose = true;
            } catch(IOException | ClassNotFoundException e) {
                e.printStackTrace();
                shouldClose = true;
            }
        }
    }

    public void setMsgHandler(Function<TextMessage, Void> msgHandler) {
        this.msgHandler = msgHandler;
    }

    private final ConnectionClient client;
    private Function<TextMessage, Void> msgHandler;

}
