package school.dom.messaging.client;

import school.dom.messaging.common.*;

import java.io.IOException;
import java.net.SocketException;

public class ConnectionClient {
    public ConnectionClient(RemoteConnection server, String username) {
        this.server = server;
        this.username = username;
    }

    public void send(Message msg) throws IOException {
        server.send(msg);
        server.flush();
    }

    public Message receive() throws IOException, ClassNotFoundException {
        return (Message) server.receive();
    }

    public boolean requestUsername() throws IOException, ClassNotFoundException {
        ConnectMessage request = new ConnectMessage(username);
        server.send(request);

        ResponseMessage response = (ResponseMessage) server.receive();
        return response.getResponseCode() == ResponseCode.OK;
    }

    public void closeConnection() {
        try {
            DisconnectMessage msg = new DisconnectMessage(username);
            server.send(msg);
            server.close();
        } catch (SocketException ignored) {
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    private final RemoteConnection server;
    private final String username;
}
