package school.dom.messaging.client;

import school.dom.messaging.common.*;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.function.Function;

public class HandlerClient implements Runnable {
    public HandlerClient(RemoteConnection server, String username) {
        this.username = username;
        this.client = new ConnectionClient(server, username);
        keyboardIn = new BufferedReader(new InputStreamReader(System.in));
        incomingMsgQueue = new LinkedBlockingQueue<>();
    }

    static final String HELP_TEXT =
            "On startup, messages will be automatically displayed as they are received.\n"
                    + "Press Enter to enable the prompt and temporarily disable automatic message display.\n"
                    + "You can then type one of the following commands:\n"
                    + "help:                      Display this message again.\n"
                    + "quit | exit:               Disconnects from server and closes the process.\n"
                    + "display:                   Displays any incoming messages that were hidden by the prompt.\n"
                    + "reset:                     Disables prompt, and re-enables automatic message display.\n"
                    + "[recipient] << [contents]: Send a message to [recipient using this syntax.\n"
                    + "                           If recipient is " + MessagingConstants.BROADCAST + ", the message\n"
                    + "                           will be forwarded to all other connected users.\n";

    @Override
    public void run() {
        // Register username to server
        boolean wasRegistered = false;
        try {
            wasRegistered = client.requestUsername();

            if(wasRegistered)
                System.out.println("The username was registered successfully.");
            else
                System.err.println("Could not obtain requested username.");
        } catch (IOException e) {
            System.err.println("A network error occurred.");
        } catch (ClassNotFoundException e) {
            System.err.println("Received invalid data from server.");
        }

        if(!wasRegistered) {
            System.err.println("Terminating process.");
            client.closeConnection();
            return;
        }

        System.out.println();
        System.out.println(HELP_TEXT);

        // Define message handler function to be passed to Listener Thread;
        // this function shares the message queue and the boolean determining its usage
        AtomicBoolean enqueueMessages = new AtomicBoolean(false);
        Function<TextMessage, Void> msgHandler = new Function<>() {
            @Override
            public Void apply(TextMessage msg) {
                if(mEnqueueMessages.get())
                    mIncomingMsgQueue.add(msg);
                else
                    System.out.println(msg.toDisplayString());
                return null;
            }

            private final BlockingQueue<TextMessage> mIncomingMsgQueue = incomingMsgQueue;
            private final AtomicBoolean mEnqueueMessages = enqueueMessages;
        };

        // Initiate communication
        Thread listener = new Thread(new ListenerClient(client, msgHandler));
        listener.start();

        // I/O loop
        boolean shouldClose = false;
        while(!shouldClose) {
            // Display new messages
            displayMessages();

            // Wait for user to press Enter if messages aren't being enqueued, then prompt for input
            if(!enqueueMessages.get()) {
                try {
                    keyboardIn.readLine();
                } catch (IOException e) {
                    System.err.println("Error occurred when reading user input. Terminating process.");
                    client.closeConnection();
                    return;
                }
            }

            // disable message output while awaiting input
            enqueueMessages.set(true);

            System.out.print("> ");
            try {
                String in = keyboardIn.readLine();
                switch (in.toLowerCase()) {
                    case "help" -> {
                        System.out.println(HELP_TEXT);
                    }
                    case "quit", "exit", "" -> {
                        shouldClose = true;
                    }
                    case "display" -> {
                        System.out.print('\r');
                        displayMessages();
                    }
                    case "reset" -> {
                        enqueueMessages.set(false);
                    }
                    default -> {
                        String[] params = in.split(" << ");
                        if (params.length >= 2) {
                            TextMessage outMsg = new TextMessage(username, params[0], params[1]);
                            client.send(outMsg);
                        } else {
                            System.err.println("Unknown command.");
                        }
                    }
                }
            } catch (IOException e) {
                System.err.println("Error occurred when reading user input. Terminating process.");
                client.closeConnection();
                return;
            }
        }

        // Cleanup resources
        client.closeConnection();
    }

    private void displayMessages() {
        while(!incomingMsgQueue.isEmpty()) {
            TextMessage msg = incomingMsgQueue.remove();
            System.out.println(msg.toDisplayString());
        }
    }

    private final String username;
    private final ConnectionClient client;
    private final BufferedReader keyboardIn;
    private final BlockingQueue<TextMessage> incomingMsgQueue;
}
