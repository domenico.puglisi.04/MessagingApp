package school.dom.messaging.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.net.ConnectException;

import school.dom.messaging.common.*;

public class MessagingClient {
    public static void main(String[] args) {
        String remoteHostname = "localhost";
        int port = MessagingConstants.PORT;

        // Setup Logging
        ROOT_LOGGER.setLevel(Level.WARNING);

        // TODO: reformat to allow more flexible argument parsing
        if(args.length >= 2) {
            remoteHostname = args[0];
            port = Integer.parseInt(args[1]);
        }

        // Prompt user for username
        String username;
        try {
            BufferedReader keyboardIn = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Please register a username: ");
            username = keyboardIn.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        RemoteConnection server;
        try {
            server = new RemoteConnection(remoteHostname, port);
        } catch (ConnectException e) {
            e.printStackTrace();
            System.err.println("ERROR: server is unreachable.");
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return; // connection MUST be active to proceed
        }

        // Start the proper application logic
        HandlerClient client = new HandlerClient(server, username);
        client.run();
    }

    private static final Logger ROOT_LOGGER = Logger.getLogger("");
}
