package school.dom.messaging.gui;

import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

import school.dom.messaging.client.ConnectionClient;
import school.dom.messaging.client.ListenerClient;
import school.dom.messaging.common.*;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;

import java.io.IOException;
import java.net.ConnectException;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class GuiController {

    // Setup items
    @FXML public TextField usernameField;
    @FXML public TextField hostnameField;
    @FXML public TextField portField;

    @FXML public Button connectButton;
    @FXML public Button disconnectButton;

    // Input items
    // @FXML public ListView<String> messageListView;
    @FXML public TextField recipientField;
    @FXML public TextField inputField;
    @FXML public Button sendButton;

    // Application areas
    @FXML public AnchorPane setupArea;
    @FXML public AnchorPane inputArea;
    @FXML public TableView<MessageTableEntry> displayTable;
    @FXML public TableColumn<MessageTableEntry, String> dateColumn;
    @FXML public TableColumn<MessageTableEntry, String> timeColumn;
    @FXML public TableColumn<MessageTableEntry, String> userColumn;
    @FXML public TableColumn<MessageTableEntry, String> messageColumn;

    public void onStartup() {
        // Configures which properties are displayed by each column
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
        userColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        messageColumn.setCellValueFactory(new PropertyValueFactory<>("message"));
    }

    public void onConnect(ActionEvent actionEvent) {
        String username = usernameField.getText();
        String hostname = hostnameField.getText();
        int port = Integer.parseInt(portField.getText());

        try {
            cc = new ConnectionClient(new RemoteConnection(hostname, port), username);
        } catch (ConnectException e) {
            e.printStackTrace();
            System.err.println("Specified server is unreachable.");
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try {
            if (cc.requestUsername()) {
                System.out.println("Username registered successfully.");
                listenerClient = new Thread(new ListenerClient(cc, onMsgReceive));
                listenerClient.start();
            } else {
                throw new IOException("Requested username was denied");
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Could not register username; input might be invalid or server full.");
            onDisconnect(null);
            return;
        }

        // Disable setup and enable input after opening the connection
        // setupArea.setDisable(true);
        inputArea.setDisable(false);
        connectButton.setDisable(true);

        // and then force the Disconnect button to be active
        disconnectButton.setDisable(false);
    }

    public void onDisconnect(ActionEvent actionEvent) {
        cc.closeConnection();
        listenerClient.interrupt();

        // Re-enable setup after closing the connection
        // setupArea.setDisable(false);
        inputArea.setDisable(true);
        connectButton.setDisable(false);

        // and disable the Disconnect button once again
        disconnectButton.setDisable(true);
    }

    public void onSend(ActionEvent actionEvent) {
        String text = inputField.getText();
        String usernameTo = recipientField.getText();

        if(usernameTo.isEmpty())
            usernameTo = MessagingConstants.BROADCAST;

        if(text.isEmpty())
            return; // cannot send message with empty body

        try {
            cc.send(new TextMessage(cc.getUsername(), usernameTo, text));
        } catch(IOException e) {
            e.printStackTrace();
        }

        // Re-set input field value after sending a message
        inputField.clear();
    }


    public void onEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            onSend(null);
        }
    }

    private ConnectionClient cc;
    private Thread listenerClient;
    private final Function<TextMessage, Void> onMsgReceive = new Function<TextMessage, Void>() {
        @Override
        public Void apply(TextMessage textMessage) {
            final MessageTableEntry entry = new MessageTableEntry(
                    textMessage.getDateTime().format(dateFmt),
                    textMessage.getDateTime().format(timeFmt),
                    "@" + textMessage.getUsernameFrom(),
                    textMessage.getBuffer()
            );

            displayTable.getItems().add(entry);
            displayTable.refresh();
            return null;
        }

        private final DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        private final DateTimeFormatter timeFmt = DateTimeFormatter.ofPattern("HH:mm:ss O");
    };
}
