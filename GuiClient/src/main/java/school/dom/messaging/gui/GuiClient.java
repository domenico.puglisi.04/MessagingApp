package school.dom.messaging.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GuiClient extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        final FXMLLoader fxmlLoader = new FXMLLoader(GuiClient.class.getResource("gui-view.fxml"));
        final Parent root = fxmlLoader.load();
        controller = fxmlLoader.getController();
        controller.onStartup();

        Scene scene = new Scene(root);
        stage.setResizable(true);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        controller.onDisconnect(null);
        super.stop();
    }

    GuiController controller;
}
