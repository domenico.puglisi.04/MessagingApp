module school.dom.messaging.gui {
    requires javafx.fxml;
    requires javafx.controls;

    requires school.dom.messaging.common;
    requires school.dom.messaging.client;

    opens school.dom.messaging.gui to javafx.graphics;
    exports school.dom.messaging.gui;
}
